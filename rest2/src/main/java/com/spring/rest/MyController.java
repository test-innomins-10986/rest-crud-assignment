package com.spring.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/rest")
public class MyController {
	
	@GetMapping("/get")
	public ResponseEntity<String> getExample() {
		return new ResponseEntity<String>("Get Example", HttpStatus.OK);
	}

	@GetMapping("/getvar/{id}")
	public ResponseEntity<String> getExampleWithVariable(@PathVariable String id) {
		return new ResponseEntity<String>("Get With Variable Example" + id, HttpStatus.OK);
	}

	@PostMapping("/post")
	public ResponseEntity<String> postExample() {
		return new ResponseEntity<String>("Post Example", HttpStatus.OK);
	}

	@PutMapping("/put")
	public @ResponseBody ResponseEntity<String> putExample() {
		return new ResponseEntity<String>("Put Example", HttpStatus.OK);
	}

	@DeleteMapping("/delete")
	public ResponseEntity<String> deleteExample() {
		return new ResponseEntity<String>("Delete Example", HttpStatus.OK);
	}

	@PatchMapping("/patch")
	public ResponseEntity<String> patchPerson() {
		return new ResponseEntity<String>("Patch Example", HttpStatus.OK);
	}
}